using System.Collections.Generic;
using Godot;

public class RootScene : Node2D
{
    [Export]
    private string _firstScene = "Menu";
    private Viewport _viewport;

    public override void _Ready()
    {
        _viewport = GetViewport();
        GetTree().Connect("screen_resized", this, "OnScreenResize");
        OnScreenResize();
        ChangeScene(_firstScene);
    }

    private void OnScreenResize()
    {
        var windowSize = OS.GetWindowSize();

        var scaleX = Mathf.Floor(windowSize.x / _viewport.Size.x);
        var scaleY = Mathf.Floor(windowSize.y / _viewport.Size.y);

        var scale = Mathf.Max(1, Mathf.Min(scaleX, scaleY));

        var diff = windowSize - (_viewport.Size * scale);
        var diffHalf = (diff * 0.5f).Floor();

        _viewport.SetAttachToScreenRect(new Rect2(diffHalf, _viewport.Size * scale));
    }

    public void ChangeScene(string scene)
    {
        var loaded = ResourceLoader.Load($"res://Levels/{scene}.tscn");
        if (loaded is PackedScene loadedScene)
        {
            var child = GetChild(1);
            RemoveChild(child);
            AddChild(loadedScene.Instance());
        }
    }
}
