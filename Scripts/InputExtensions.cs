using System.Linq;
using Godot;

public static class InputExtensions
{
    private static int[] UpCodes = new int[] { (int)KeyList.Up, (int)KeyList.W };
    private static int[] DownCodes = new int[] { (int)KeyList.Down, (int)KeyList.S };
    private static int[] LeftCodes = new int[] { (int)KeyList.Left, (int)KeyList.A };
    private static int[] RightCodes = new int[] { (int)KeyList.Right, (int)KeyList.D };
    private static int[] InteractCodes = new int[] { (int)KeyList.Space, (int)KeyList.Enter };
    private static int[] BackCodes = new int[] { (int)KeyList.Escape, (int)KeyList.Tab };

    public static InputButton GetInputButton(this InputEventKey input)
    {
        switch (input.Scancode)
        {
            case (int)KeyList.Up:
            case (int)KeyList.W:
                return InputButton.Up;
            case (int)KeyList.Down:
            case (int)KeyList.S:
                return InputButton.Down;
            case (int)KeyList.Left:
            case (int)KeyList.A:
                return InputButton.Left;
            case (int)KeyList.Right:
            case (int)KeyList.D:
                return InputButton.Right;
            case (int)KeyList.Enter:
            case (int)KeyList.Space:
                return InputButton.Interact;
            case (int)KeyList.Escape:
            case (int)KeyList.Tab:
                return InputButton.Back;
            default:
                return InputButton.None;
        }
    }

}