using Godot;
using System;

public class ArrowCollection : Node2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    private const int TOTAL_LEVELS = 5;

    [Signal]
    public delegate void OnSelectionChanged(int newSelection);

    [Signal]
    public delegate void OnLevelChanged(int newLevel);

    private int _options = 3;

    private int _currentLevel = 0;
    public int CurrentLevel
    {
        get { return _currentLevel; }
        set
        {
            _currentLevel = Mathf.Max(0, Mathf.Min(value, TOTAL_LEVELS));
            EmitSignal(nameof(OnLevelChanged), _currentLevel);
        }
    }
    private int _activeSelection;

    public int ActiveSelection
    {
        get
        { return _activeSelection; }
        private set
        {
            _activeSelection = (value + _options) % _options;
            EmitSignal(nameof(OnSelectionChanged), _activeSelection);
        }
    }

    private RootScene _parent;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        SetProcessInput(true);
        ActiveSelection = 0;
        _parent = FindParent("RootScene") as RootScene;
    }

    public override void _Input(InputEvent @event)
    {
        switch (@event)
        {
            case InputEventKey keyEvent:
                HandleKeyboardInput(keyEvent);
                break;
            default:
                // Ignore
                break;
        }
    }

    private void HandleKeyboardInput(InputEventKey keyEvent)
    {
        if (keyEvent.Pressed)
        {
            switch (keyEvent.GetInputButton())
            {
                case InputButton.Up:
                    ActiveSelection--;
                    break;
                case InputButton.Down:
                    ActiveSelection++;
                    break;
                case InputButton.Left:
                    CurrentLevel -= (ActiveSelection == 0 ? 1 : 0);
                    break;
                case InputButton.Right:
                    CurrentLevel += (ActiveSelection == 0 ? 1 : 0);
                    break;
                case InputButton.Interact:
                    ChangeScene();
                    break;
                default:
                    // Ignore
                    break;
            }
        }
    }

    private void ChangeScene()
    {
        switch (_activeSelection)
        {
            case 0:
                _parent.ChangeScene(String.Format("Level{0:D2}", _currentLevel));
                break;
            case 1:
                _parent.ChangeScene("Controls");
                break;
            case 2:
                GetTree().Quit();
                break;
        }
    }
}
