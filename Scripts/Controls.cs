using Godot;
using System;

public class Controls : Node2D
{

    RootScene _parent;
    public override void _Ready()
    {
        _parent = GetParent() as RootScene;
        SetProcessInput(true);
    }

    public override void _Input(InputEvent @event)
    {
        if (@event is InputEventKey keyEvent &&
            keyEvent.Pressed &&
            (keyEvent.GetInputButton() == InputButton.Interact || keyEvent.GetInputButton() == InputButton.Back))
        {
            _parent.ChangeScene("Menu");
        }

    }


}
