using System.Collections.Generic;
using Godot;

public static class Textures
{
    public static Texture DropTexture = ResourceLoader.Load<Texture>("res://Assets/UI/Drop.tres");
    public static Texture SwapTexture = ResourceLoader.Load<Texture>("res://Assets/UI/Swap.tres");
    public static Texture TakeTexture = ResourceLoader.Load<Texture>("res://Assets/UI/Take.tres");
    public static Texture OpenTexture = ResourceLoader.Load<Texture>("res://Assets/UI/Open.tres");
    public static Dictionary<ItemType, Texture> ItemTextures = new Dictionary<ItemType, Texture>() {
        {ItemType.NoItem, ResourceLoader.Load<Texture>("res://Assets/Items/NoItem.tres")},
        {ItemType.FireCoin, ResourceLoader.Load<Texture>("res://Assets/Items/FireCoin.tres")},
        {ItemType.WaterCoin, ResourceLoader.Load<Texture>("res://Assets/Items/WaterCoin.tres")},
        {ItemType.Key, ResourceLoader.Load<Texture>("res://Assets/Items/Key.tres")},
    };
}