using Godot;

public class CurrentItem : Sprite
{
    private Player _player;
    public override void _Ready()
    {
        _player = GetTree().Root.FindNode("Player", true, false) as Player;
        _player.Connect(nameof(Player.OnItemPickUp), this, nameof(OnItemChanged));
    }

    private void OnItemChanged(ItemType newItem, ItemType _)
    {
        Texture = Textures.ItemTextures[newItem];
    }

}
