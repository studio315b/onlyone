using Godot;
using System;

public class Player : KinematicBody2D
{
    [Signal]
    public delegate void OnItemPickUp(ItemType newItemType, ItemType oldItemType);

    [Export]
    private float Speed = 40f;

    private ItemType _currentItem = ItemType.NoItem;

    public bool HasItem => _currentItem != ItemType.NoItem;
    public bool HasKey => _currentItem == ItemType.Key;

    public ItemType Trade(ItemType newItem)
    {
        var oldItem = _currentItem;
        _currentItem = newItem;
        EmitSignal(nameof(OnItemPickUp), newItem, oldItem);
        return oldItem;
    }

    public override void _Ready()
    {
        SetPhysicsProcess(true);
    }

    public override void _PhysicsProcess(float delta)
    {
        var up = Input.IsKeyPressed((int)KeyList.W) || Input.IsKeyPressed((int)KeyList.Up)
         ? 1 : 0;
        var down = Input.IsKeyPressed((int)KeyList.S) || Input.IsKeyPressed((int)KeyList.Down) ? 1 : 0;
        var left = Input.IsKeyPressed((int)KeyList.A) || Input.IsKeyPressed((int)KeyList.Left) ? 1 : 0;
        var right = Input.IsKeyPressed((int)KeyList.D) || Input.IsKeyPressed((int)KeyList.Right) ? 1 : 0;
        var direction = new Vector2(
            right - left,
            down - up
            ).Normalized() * Speed;
        MoveAndSlide(direction, Vector2.Zero);
    }
}
