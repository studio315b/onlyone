
using Godot;
using System;

public class SelectionArrow : Sprite
{
    private const float ANIMATION_SPEED = 0.5f;
    private static Texture[] Frames => new Texture[] {
        ResourceLoader.Load<Texture>("res://Assets/Arrows/SelectedArrow.tres"),
        ResourceLoader.Load<Texture>("res://Assets/Arrows/SelectedArrow2.tres"),
    };


    [Export]
    private int _activeIndex;

    private int _frame = 0;

    private float _now = 0f;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        var collection = FindParent("ArrowCollection") as ArrowCollection;
        collection.Connect(nameof(ArrowCollection.OnSelectionChanged), this, nameof(OnSelectionChanged));
        Visible = false;
        SetProcess(true);
    }

    private void OnSelectionChanged(int newSelection)
    {
        Visible = _activeIndex == newSelection;
    }
    public override void _Process(float delta)
    {
        _now += delta;
        if (_now > ANIMATION_SPEED)
        {
            _frame = (_frame + 1) % Frames.Length;
            Texture = Frames[_frame];
            _now -= ANIMATION_SPEED;
        }
    }
}
