using Godot;
using System;

public class LevelMenu : Node2D
{

    [Export]
    private int _levelNumber;

    [Export]
    private bool _lastLevel;

    private Node2D _menu;
    private Node2D _level;
    private bool _inMenu;

    private bool InMenu
    {
        get { return _inMenu; }
        set
        {
            _inMenu = value;
            _menu.Visible = _inMenu;
            _level.Visible = !_inMenu;
            GetTree().SetPause(_inMenu);
        }
    }

    private RootScene _parent;
    public override void _Ready()
    {
        SetProcessInput(true);
        _menu = GetChild(0) as Node2D;
        _level = GetChild(1) as Node2D;
        _parent = FindParent("RootScene") as RootScene;
        InMenu = true;
    }

    public override void _Input(InputEvent @event)
    {
        if (@event is InputEventKey keyEvent && keyEvent.Pressed)
        {
            if (keyEvent.Scancode == (int)KeyList.F5)
            {
                RestartLevel();
            }
            else if (!InMenu && keyEvent.GetInputButton() == InputButton.Back)
            {
                InMenu = true;
            }
            else if (InMenu)
            {
                switch (keyEvent.GetInputButton())
                {
                    case InputButton.Back:
                        QuitToMenu();
                        break;
                    case InputButton.Interact:
                        InMenu = false;
                        break;
                }
            }
        }
    }


    public void CompleteLevel()
    {
        if (!_lastLevel)
        {
            _parent.ChangeScene(String.Format("Level{0:D2}", _levelNumber + 1));
        }
        else
        {
            QuitToMenu();
        }
    }

    public void RestartLevel()
    {
        _parent.ChangeScene(String.Format("Level{0:D2}", _levelNumber));
    }

    public void QuitToMenu()
    {
        _parent.ChangeScene("Menu");
    }
}
