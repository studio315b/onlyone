using Godot;
using System;

public class Pedestal : KinematicBody2D
{
    private static Texture WithoutItem = ResourceLoader.Load<Texture>("res://Assets/Terrain/Pedestal.tres");
    private static Texture WithItem = ResourceLoader.Load<Texture>("res://Assets/Terrain/PedestalWithTreasure.tres");
    [Export]
    private ItemType _item;

    private Player _player;
    private ActionPair _pair;
    private Sprite _child;

    private bool _inRange;

    private bool InRange
    {
        get { return _inRange; }
        set
        {
            _inRange = value;
            SetProcessInput(value);
            if (value)
            {
                _pair.TouchedPedistal(_item, _player.HasItem);
            }
            else
            {
                _pair.Leave();
            }
        }
    }

    private static Rect2 ItemRegion = new Rect2(new Vector2(0, 32), new Vector2(16, 16));
    private static Rect2 NoItemRegion = new Rect2(new Vector2(0, 16), new Vector2(16, 16));

    public override void _Ready()
    {
        _child = GetChild<Sprite>(0);
        _player = GetTree().Root.FindNode("Player", true, false) as Player;
        _pair = GetTree().Root.FindNode("ActionPair", true, false) as ActionPair;
        SetProcess(true);
        SetTexture();
    }

    private void SetTexture()
    {
        if (_item == ItemType.NoItem)
        {
            _child.Texture = WithoutItem;
        }
        else
        {
            _child.Texture = WithItem;

        }
    }

    public override void _Process(float delta)
    {
        if (!InRange && _player.Position.DistanceTo(Position) < Constants.INTERACTION_RANGE)
        {
            InRange = true;
        }
        else if (InRange && _player.Position.DistanceTo(Position) > Constants.INTERACTION_RANGE)
        {
            InRange = false;
        }
    }

    public override void _Input(InputEvent @event)
    {
        if (@event is InputEventKey keyEvent
            && keyEvent.Pressed
            && keyEvent.GetInputButton() == InputButton.Interact
            && InRange)

        {
            _item = _player.Trade(_item);
            _pair.TouchedPedistal(_item, _player.HasItem);
            SetTexture();
        }
    }
}
