using Godot;
using System;

public class Exit : Sprite
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";


    // Called when the node enters the scene tree for the first time.
    private Player _player;
    private LevelMenu _level;

    public override void _Ready()
    {
        _player = GetTree().Root.FindNode("Player", true, false) as Player;
        _level = GetTree().Root.FindNode("Level", true, false) as LevelMenu;
        GD.Print(_level != null);
        SetProcess(true);
    }

    public override void _Process(float delta)
    {
        if (_player.Position.DistanceTo(Position) < 8)
        {
            _level.CompleteLevel();
        }
    }
}
