using Godot;
using System;

public class Fire : KinematicBody2D
{
    private Player _player;
    private CollisionShape2D _collision;
    public override void _Ready()
    {
        _player = GetTree().Root.FindNode("Player", true, false) as Player;
        _player.Connect(nameof(Player.OnItemPickUp), this, nameof(OnItemChange));
        _collision = GetChild<CollisionShape2D>(0);
    }

    private void OnItemChange(ItemType newItem, ItemType _)
    {
        _collision.Disabled = newItem == ItemType.FireCoin;
    }

}
