public enum ItemType
{
    NoItem,
    FireCoin,
    WaterCoin,
    EarthCoin,
    WindCoin,
    Key,
    PushRing,
    PullRing,
}