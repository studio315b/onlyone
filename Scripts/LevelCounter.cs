using Godot;
using System;

public class LevelCounter : Node2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    private static Texture[] digits = new Texture[] {
        ResourceLoader.Load<Texture>("res://Assets/Numbers/0.tres"),
        ResourceLoader.Load<Texture>("res://Assets/Numbers/1.tres"),
        ResourceLoader.Load<Texture>("res://Assets/Numbers/2.tres"),
        ResourceLoader.Load<Texture>("res://Assets/Numbers/3.tres"),
        ResourceLoader.Load<Texture>("res://Assets/Numbers/4.tres"),
        ResourceLoader.Load<Texture>("res://Assets/Numbers/5.tres"),
        ResourceLoader.Load<Texture>("res://Assets/Numbers/6.tres"),
        ResourceLoader.Load<Texture>("res://Assets/Numbers/7.tres"),
        ResourceLoader.Load<Texture>("res://Assets/Numbers/8.tres"),
        ResourceLoader.Load<Texture>("res://Assets/Numbers/9.tres"),
    };
    private Sprite _tens;
    private Sprite _ones;

    public override void _Ready()
    {
        _tens = GetChild<Sprite>(0);
        _ones = GetChild<Sprite>(1);
        var parent = FindParent("ArrowCollection") as ArrowCollection;
        parent.Connect(nameof(ArrowCollection.OnLevelChanged), this, nameof(OnLevelChanged));
    }

    private void OnLevelChanged(int newLevel)
    {
        var tIndex = newLevel / 10;
        var oIndex = newLevel % 10;

        _tens.Texture = digits[tIndex];
        _ones.Texture = digits[oIndex];
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }
}
