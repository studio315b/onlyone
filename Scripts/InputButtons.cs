public enum InputButton
{
    None,
    Up,
    Down,
    Left,
    Right,
    Interact,
    Back
}