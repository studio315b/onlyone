using Godot;
using System;

public class Lock : KinematicBody2D
{

    private Player _player;
    private ActionPair _pair;

    private bool _inRange;

    private bool InRange
    {
        get { return _inRange; }
        set
        {
            _inRange = value;
            SetProcessInput(value);
            if (value)
            {
                _pair.TouchedLock(_player.HasKey);
            }
            else
            {
                _pair.Leave();
            }
        }
    }

    private static Rect2 ItemRegion = new Rect2(new Vector2(0, 32), new Vector2(16, 16));
    private static Rect2 NoItemRegion = new Rect2(new Vector2(0, 16), new Vector2(16, 16));

    public override void _Ready()
    {
        _player = GetTree().Root.FindNode("Player", true, false) as Player;
        _pair = GetTree().Root.FindNode("ActionPair", true, false) as ActionPair;
        SetProcess(true);
    }

    public override void _Process(float delta)
    {
        if (!InRange && _player.Position.DistanceTo(Position) < Constants.INTERACTION_RANGE)
        {
            InRange = true;
        }
        else if (InRange && _player.Position.DistanceTo(Position) > Constants.INTERACTION_RANGE)
        {
            InRange = false;
        }
    }

    public override void _Input(InputEvent @event)
    {
        if (@event is InputEventKey keyEvent
            && keyEvent.Pressed
            && keyEvent.GetInputButton() == InputButton.Interact
            && InRange
            && _player.HasKey)

        {
            _player.Trade(ItemType.NoItem);
            _pair.Leave();
            GetParent().RemoveChild(this);
        }
    }
}
