using Godot;
using System.Collections.Generic;

public class ActionPair : Node2D
{



    private Sprite _action;
    private Sprite _other;
    public override void _Ready()
    {
        _action = GetChild<Sprite>(0);
        _other = GetChild<Sprite>(1);
        Leave();
    }

    public void Leave()
    {
        _action.Texture = null;
        _other.Texture = null;
    }

    public void TouchedPedistal(ItemType item, bool playerHasItem)
    {
        if (playerHasItem)
        {
            if (item == ItemType.NoItem)
            {
                _action.Texture = Textures.DropTexture;
                _other.Texture = null;
            }
            else
            {
                _action.Texture = Textures.SwapTexture;
                _other.Texture = Textures.ItemTextures[item];
            }
        }
        else if (item != ItemType.NoItem)
        {
            _action.Texture = Textures.TakeTexture;
            _other.Texture = Textures.ItemTextures[item];
        }
    }

    public void TouchedLock(bool hasKey)
    {
        if (hasKey)
        {
            _action.Texture = Textures.OpenTexture;
        }
    }


}
