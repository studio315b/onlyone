
using Godot;
using System;
using System.Collections.Generic;

public class IterationArrow : Sprite
{

    private InputButton InputButton
    {
        get
        {
            switch (_arrowType)
            {
                case ArrowType.Increase:
                    return InputButton.Right;
                case ArrowType.Decrease:
                    return InputButton.Left;
                default:
                    throw new ArgumentException("Unexpected Arrow Type");
            }
        }
    }

    [Export]
    private ArrowType _arrowType;

    [Export]
    private int _activeIndex;
    [Export]
    private Texture _inactiveTexture;
    [Export]
    private Texture _activeTexture;
    private Texture[] _textures => new Texture[] { _inactiveTexture, _activeTexture };




    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        var collection = FindParent("ArrowCollection") as ArrowCollection;
        collection.Connect(nameof(ArrowCollection.OnSelectionChanged), this, nameof(OnSelectionChanged));
        Texture = _textures[0];
    }
    private void OnSelectionChanged(int newSelection)
    {

        SetProcessInput(_activeIndex == newSelection);
    }

    #region Input

    public override void _Input(InputEvent @event)
    {
        switch (@event)
        {
            case InputEventKey keyEvent:
                OnKeyInput(keyEvent);
                break;
            default:
                break;
        }
    }

    private void OnKeyInput(InputEventKey keyEvent)
    {
        if (keyEvent.GetInputButton() == InputButton)
        {
            Texture = _textures[keyEvent.Pressed ? 1 : 0];
        }
    }

    private void DecreaseOnInput(InputEvent @event)
    {
        switch (@event)
        {
            case InputEventKey keyEvent:
                OnKeyInput(keyEvent);
                break;
            default:
                break;
        }
    }

    #endregion

}
